<?php
/**
 * Moyenne des moutons
 *
 * @param array $moutons
 * @return float average
 */
function average(array $moutons): float{
    $sumValMoutons=0;
    foreach ($moutons as $mouton) {
        $sumValMoutons += $mouton[1];
    }
    return $sumValMoutons/count($moutons);
}