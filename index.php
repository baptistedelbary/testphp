<?php
require('function.php');

echo "Test PHP By Baptiste Delbary<br/>";

// Un tableau de moutons
$moutons = [['Danny', 75], ['Richard',60]];
// J'ajoute un mouton
array_push($moutons, ['Gérard',120]);

// Je calcule la moyenne de la valeur de mes moutons
echo "Moyenne de la valeur de mes ". count($moutons)." moutons : ".average($moutons)."<br/>";

// Ajout de 100 moutons aléatoires
$chaine = "abcdefghijklmnpqrstuvwxyABCDEFGHIJKLMNOPQRSUTVWXYZ";
$nbChars = strlen($chaine);

for ($j=0; $j < 100; $j++) { 
	$randNameMouton = "";
	for($k = 0; $k < rand(3,15); $k++)
    {
        $randNameMouton .= $chaine[rand(0, ($nbChars-1))];
    }
	array_push($moutons, [$randNameMouton, rand(10,200)]);
}

// Je calcule à nouveau la moyenne
echo "Moyenne de la valeur de mes ". count($moutons)." moutons : ".average($moutons)."<br/>";